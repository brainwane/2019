---
name: Kelley Robinson
talks:
- "A Tale of Two Factors: A Deep Dive into Two Factor Authentication"
---
Kelley works on the Account Security team at Twilio, helping developers
manage and secure customer identity in their software applications.
Previously she worked in a variety of API platform and data engineering
roles at startups in San Francisco. She believes in making technical
concepts, especially security, accessible and approachable for new
audiences. In her spare time, Kelley is an avid home cook and greatly enjoys
reorganizing her Brooklyn kitchen to accommodate completely necessary small
appliance purchases.
