---
name: Joseph Willi
talks:
- "Using Dash to Create Interactive Web Apps for Non-Technical Audiences"
---
Joseph Willi works as a Research Engineer with the Underwriters Laboratories
Firefighter Safety Research Institute (FSRI), a group dedicated to
increasing firefighter knowledge to reduce injuries and deaths in the fire
service and communities they serve. Along with other members of the FSRI
team, Joe regularly utilizes Python for his work, most frequently during the
data acquisition and analysis portions of research projects. Additionally,
he has incorporated Python into projects involving image analysis and object
detection applications. Joseph holds a bachelor's degree in General
Engineering from the University of Illinois at Urbana-Champaign as well as a
master's degree in Fire Protection Engineering from the University of
Maryland, College Park.
