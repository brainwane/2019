---
duration: 40
presentation_url:
room:
slot:
speakers:
- Mike Pirnat
title: "Dungeons \u0026 Dragons \u0026 Python: Epic Adventures with Prompt-Toolkit and Friends"
type: talk
video_url:
---
Embark on an epic adventure through the twisty passageways of [a Python
application developed to help Dungeon Masters run Dungeons & Dragons
sessions](https://github.com/mpirnat/dndme). You’ll be joined in your quest
by mighty allies such as [Prompt-Toolkit](https://python-prompt-
toolkit.readthedocs.io/en/master/),
[Attrs](https://www.attrs.org/en/stable/),
[Click](https://click.palletsprojects.com/en/7.x/), and
[TOML](https://pypi.org/project/toml/) as you brave the perils of
application structure, command completion, dynamic plugin discovery, data
modeling, turn tracking, and maybe even some good old-fashioned dice
rolling. Treasure and glory await!
