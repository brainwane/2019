---
duration: 25
presentation_url:
room:
slot:
speakers:
- Darshan Markandaiah
title: "The magic of Python"
type: talk
video_url:
---
Python has many built in magic functions that are used internally by classes
for certain actions. For example, adding two numbers calls the `__add__`
method on one of the numbers  and iterating over a list calls the
`__iter__`  method on that list object. I will expand on this Duck Typing
principle in this talk and enumerate over a range of magic methods that you
can add to your classes to have a cleaner codebase. For each category of of
magic methods that I outline, I will provide examples for practical use.
