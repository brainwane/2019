---
duration: 40
presentation_url:
room:
slot:
speakers:
- Alizishaan Khatri
title: "Serverless Deep Learning with Python"
type: talk
video_url:
---
Do you marvel at the idea of production-grade Deep Learning that can scale
infinitely in nearly constant time?

Using an NLP application as an example, you will learn how to design, build
and deploy deep learning systems using a serverless computing platform
(Function As A Service).

PS: Talk contains code!
