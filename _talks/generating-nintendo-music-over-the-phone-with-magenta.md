---
duration: 25
presentation_url:
room:
slot:
speakers:
- Sam Agnew
title: "Generating Nintendo Music Over the Phone with Magenta"
type: talk
video_url:
---
In this live coded quest we will walk through how to generate music with
Magenta using a neural network trained on the soundtracks from classic
Nintendo games, and create a Flask application which uses the Twilio API to
provide a phone number people can call to listen to this music in real time.
