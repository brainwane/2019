---
duration: 40
presentation_url:
room:
slot:
speakers:
- Hynek Schlawack
title: "Maintaining a Python Project When It\u2019s Not Your Job"
type: talk
video_url:
---
PyPI is a gold mine of great packages but those packages have to be written
first.  More often than not, projects that millions of people depend on are
written and maintained by only one person.  If you’re unlucky, that person
is you!

So how do you square delivering a *high quality* Python package you can be
proud of and having only limited time at your disposal?  The answer is not
“try harder,” the answer is to **do less**.

This talk will help you get there by talking about how you can make your
life easier, remove causes of friction with your contributors, and empower
said contributors to take over tasks that you can’t make time for anymore.
